let () = Loge.set_verbosity "fibonacci" Trace

let rec fibonacci limit a b =
  if limit <= 0 then begin
    Loge.log ~level:Trace ~section:"fibonacci" "Finished" ;
    b
  end else begin
    let c = a + b in
    Loge.log ~level:Trace ~section:"fibonacci" (Printf.sprintf "Found %d" c) ;
    fibonacci (limit - 1) b c
  end

let rec square limit x =
  if limit <= 0 then begin
    Loge.log ~level:Trace ~section:"square" "Finished" ;
    x
  end else if x <= 0 then begin
    Loge.log ~level:Fatal ~section:"square" "Overflow detected" ;
    exit 1
  end else begin
    let x = x * x in
    Loge.log ~level:Trace ~section:"square" (Printf.sprintf "Found %d" x) ;
    square (limit - 1) x
  end

let main () =
  let f = fibonacci 10 0 1 in
  let () = Printf.printf "Fibonacci: %d\n" f in
  let s = square 10 234 in
  let () = Printf.printf "Square: %d\n" s in
  ()

let () = Loge_domain.with_domains_log_handler ~queue_size_bound:1 main ()
