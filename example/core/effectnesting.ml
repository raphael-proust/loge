module Rand : sig
  type _ Effect.t += Rand : int -> int Effect.t
  val get : int -> int
  val set_verbosity : Loge.level -> unit
  val handler : 'a Effect.Deep.effect_handler
end = struct
  type _ Effect.t += Rand : int -> int Effect.t
  let get bound = Effect.perform (Rand bound)
  let section = "rand"
  let set_verbosity level = Loge.set_verbosity section level
  let () = Random.self_init ()
  let handler =
    let effc
      : type b. b Effect.t -> ((b, 'a) Effect.Deep.continuation -> 'a) option
      = function
        | Rand b -> Some (fun k ->
            if b <= 0 then begin
              Loge.log ~level:Errors ~section "Cannot rand a negative or null";
              Effect.Deep.discontinue k (Invalid_argument "Rand.handler")
            end else begin
              Loge.log ~level:Trace ~section "Consuming entropy like there is no tomorrow" ;
              Effect.Deep.continue k (Random.int b)
            end
          )
        | _ -> None
    in
    { Effect.Deep.effc }
end

let () = Rand.set_verbosity Errors
let section = "main"
let () = Loge.set_verbosity section Information

let rec pickpick n v =
  if n <= 0 then begin
    Loge.log ~level:Information ~section "Found value" ;
    v
  end else begin
    let v = Rand.get v in
    Loge.log ~level:Notification ~section (Printf.sprintf "Continuing with %d" v);
    pickpick (n - 1) v
  end

let main () =
  let a = pickpick 3 10 in
  let b = pickpick 3 10 in
  if a < b then
    Printf.printf "Left\n"
  else if a > b then
    Printf.printf "Right\n"
  else (* a = b *)
    Printf.printf "Bingo!\n"

let () =
  Loge.with_stderr_log_handler (fun () -> Effect.Deep.try_with main () Rand.handler) ()
