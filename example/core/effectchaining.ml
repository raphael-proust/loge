module Rand : sig
  type _ Effect.t += Rand : int -> int Effect.t
  val get : int -> int
  val handler : 'a Effect.Deep.effect_handler
end = struct
  type _ Effect.t += Rand : int -> int Effect.t
  let get bound = Effect.perform (Rand bound)
  let () = Random.self_init ()
  let handler =
    let effc
      : type a b. b Effect.t -> ((b, a) Effect.Deep.continuation -> a) option
      = function
        | Rand b -> Some (fun k -> Effect.Deep.continue k (Random.int b))
        | _ -> None
    in
    { Effect.Deep.effc }
end

let section = "main"
let () = Loge.set_verbosity section Information

let rec picks acc =
  if List.compare_length_with acc 3 >= 0 then begin
    Loge.log ~level:Information ~section "Found 3 values" ;
    acc
  end else
    let v = Rand.get 10 in
    if List.mem v acc then begin
      Loge.log ~level:Trace ~section "Conflict, ignoring value";
      picks acc
    end else begin
      Loge.log ~level:Notification ~section (Printf.sprintf "New value added: %d" v);
      picks (v :: acc)
    end

let main () =
  let a = picks [] in
  let b = picks [] in
  if List.exists (fun v -> List.mem v a) b then
    Printf.printf "Collision!\n"
  else
    Printf.printf "No collision!\n"

let () =
  Effect.Deep.try_with (Loge.with_stderr_log_handler main) () Rand.handler
