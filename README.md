# Loge

Loge is a logging system based on effects.

## Purpose

The main purpose of Loge is to experiment with effects and handlers.

## Reading guide

The Loge library is located in `src/core/`. It defines:
- some basic types and values related to logging (verbosity level management),
- a `Log` effect which is intended to be performed for logging,
- two separate handlers for the `Log` effect:
    - `with_null_log_handler` runs a computation ignoring all logging effects,
    - `with_stderr_log_handler` runs a computation logging to stderr.

The handlers are provided as simple function wrappers: `('a -> 'b) -> 'a -> 'b`.

At one point of the experimentation, the handlers were presented as
`Effect.Deep.effect_handler` and it was the responsibility of the user to
compose handlers together if need be (e.g., `example/core/effectchaining.ml` and
`example/core/effectnesting.ml`). However, this proved impossible to extend to
the handlers that need some state management (setup and teardown in particular).

The Loge-domain library is located in `src/domain/`. It defines a single handler
for the `Loge.Log` effect. This handler pushes the log message into a queue. The
messages pushed to the queue are consumed by a task running in a separate
domain.

This handler requires setup (the domain channel serving as a queue, the domain
pool to attach the task to) and teardown (waiting for the logging to end,
tearing down the domain pool).

The `example/domain/parlog.ml` file shows a simple use of this domain-based
handler. Note that the example is the exact same as `src/example/justlog.ml`
(apart from the choice of handler and being built against `loge-domain`).
