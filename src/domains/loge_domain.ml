let with_domains_log_handler ?queue_size_bound comp arg =

  let chan =
    match queue_size_bound with
    | None -> Domainslib.Chan.make_unbounded ()
    | Some n -> Domainslib.Chan.make_bounded n
  in
  let pool = Domainslib.Task.setup_pool ~name:"loge" ~num_additional_domains:1 () in

  let rec task () =
    match Domainslib.Chan.recv chan with
    | Some (section, msg) ->
        Printf.eprintf "%s -- %s\n" section msg;
        (task [@ocaml.tailcall]) ()
    | None ->
        ()
  in

  let promise = Domainslib.Task.async pool task in

  let effc
    : type a b. b Effect.t -> ((b, a) Effect.Deep.continuation -> a) option
    = function
      | Loge.Log (level, section, msg) -> Some (fun k ->
          if Loge.test_verbosity_level section level then
            Domainslib.Chan.send chan (Some (section, msg));
          Effect.Deep.continue k ()
          )
      | _ -> None
  in

  let retc v =
    Domainslib.Chan.send chan None;
    Domainslib.Task.await pool promise;
    flush_all ();
    Domainslib.Task.teardown_pool pool;
    v
  in

  Effect.Deep.match_with comp arg
    { retc; exnc = (fun e -> raise e); effc }
