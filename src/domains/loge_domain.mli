(** [with_domains_log_handler comp arg] is [comp arg] but all the [Log] effect
    are passed to a logging backend that runs in a separate domain.

    This is a batteries included backend which automatically sets up a separate
    domain and a communication channel. It also ensures that the logging queue
    is closed and emptied before returning.

    @param [queue_size_bound] limits the number of unprocessed logging requests
    which are stored. If the queue becomes full, then the next [Log] effect
    performed will block until there is room in the queue.

    @handle Loge.Log
*)
val with_domains_log_handler : ?queue_size_bound:int -> ('a -> 'b) -> 'a -> 'b
