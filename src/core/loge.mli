type level =
  | Fatal
  | Errors
  | Warnings
  | Notification
  | Information
  | Debug
  | Trace

type section = string

(** [set_verbosity s l] sets the verbosity of section [s] to level [l]. Once
    this is called, all log messages from levels [Fatal] up to and including [l]
    are actually handled by the installed effect handler; and all log messages
    from levels [l+1] and above are ignored. *)
val set_verbosity : section -> level -> unit

(** [true] means it should be logged, [false] means it should be ignored *)
val test_verbosity_level : section -> level -> bool

type _ Effect.t += Log : (level * section * string) -> unit Effect.t

(** @perform Log *)
val log : level:level -> section:section -> string -> unit

(** @handle Log doing nothing at all *)
val with_null_log_handler : ('a -> 'b) -> 'a -> 'b

(** @handle Log printing the message on stderr. This handler is blocking: your
    program doesn't make progress until the printing is done. *)
val with_stderr_log_handler : ('a -> 'b) -> 'a -> 'b
