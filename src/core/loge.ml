type level =
  | Fatal
  | Errors
  | Warnings
  | Notification
  | Information
  | Debug
  | Trace

type section = string

module SectionMap = Map.Make (String)

let default_verbosity_level = Errors

let verbosity_settings = ref SectionMap.empty

let set_verbosity section level =
  verbosity_settings := SectionMap.add section level !verbosity_settings

let get_verbosity section =
  Option.value ~default:default_verbosity_level
  @@ SectionMap.find_opt section !verbosity_settings

let test_verbosity_level section level =
  let set_level = get_verbosity section in
  level <= set_level

type _ Effect.t += Log : (level * section * string) -> unit Effect.t

let log ~level ~section msg =
  Effect.perform (Log (level, section, msg))

let with_null_log_handler comp arg =
  let effc
    : type a b. b Effect.t -> ((b, a) Effect.Deep.continuation -> a) option
    = function
      | Log (_, _, _) -> Some (fun k -> Effect.Deep.continue k ())
      | _ -> None
  in
  Effect.Deep.try_with comp arg { effc }

let with_stderr_log_handler comp arg =
  let effc
    : type a b. b Effect.t -> ((b, a) Effect.Deep.continuation -> a) option
    = function
      | Log (level, section, msg) -> Some (fun k ->
          if test_verbosity_level section level then
            Printf.eprintf "%s -- %s\n" section msg;
          Effect.Deep.continue k ()
          )
      | _ -> None
  in
  Effect.Deep.try_with comp arg { effc }
